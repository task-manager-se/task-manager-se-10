package ru.zolov.tm.command.project;

import java.util.Comparator;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.CommandCorruptException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public final class ProjectDisplayCommand extends AbstractCommand {

  private final String name = "project-list";
  private final String description = "Display project list";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return false;
  }

  @Override
  public void execute()
  throws CommandCorruptException, EmptyStringException, EmptyRepositoryException {
    @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
    @NotNull final String userName = serviceLocator.getUserService().getCurrentUser().getLogin();
    System.out.println("Please choose sort type. Enter: \n date-create \n date-start \n date-finish \n status");
    @NotNull final String comparatorName = serviceLocator.getTerminalService().nextLine();
    final Comparator<AbstractGoal> comparator = serviceLocator.getTerminalService()
                                                              .getComparator(comparatorName);
    if (comparator == null) throw new CommandCorruptException();
    final List<Project> list = serviceLocator.getProjectService().sortBy(userId, comparator);
    serviceLocator.getTerminalService().performList(list, userName);
  }

  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
