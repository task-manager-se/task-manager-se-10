package ru.zolov.tm.command.data.bin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Domain;
import ru.zolov.tm.enumerated.PathEnum;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class DataBinSaveCommand extends AbstractCommand {

  private final String name = "datasave-bin";
  private final String description = "Save data to binary file";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return true;
  }

  @Override
  public void execute()
      throws
      IOException,
      EmptyStringException,
      EmptyRepositoryException {
    System.out.println("Save data to file.");
    @NotNull
    final Domain domain = new Domain();
    serviceLocator.getDomainService().save(domain);
    @Nullable Path path = Paths.get(PathEnum.BIN.getPath());
    if (path == null) return;
    if (Files.notExists(path.getParent())) Files.createDirectory(path.getParent());
    @Nullable File file = path.toFile();
    if (file == null) return;
    @Cleanup
    FileOutputStream fos = new FileOutputStream(file);
    @Cleanup
    ObjectOutputStream oos = new ObjectOutputStream(fos);
    oos.writeObject(domain);
    System.out.println("DONE!");
  }

  @Override
  public RoleType[] roles() { return new RoleType[]{RoleType.ADMIN, RoleType.USER}; }
}
