package ru.zolov.tm.repository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.IRepository;
import ru.zolov.tm.entity.AbstractEntity;

abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

  Map<String, E> storage = new LinkedHashMap<>();

  abstract E persist(@NotNull final E entity);

  abstract void merge(@NotNull final E entity);


  @NotNull
  public List<E> findAll() {
    List<E> list = new ArrayList<>(storage.values());
    return list;
  }

  public void load(@NotNull List<E> list) {
    for (@NotNull final E entity : list) {
      storage.put(entity.getId(), entity);
    }
  }

}
