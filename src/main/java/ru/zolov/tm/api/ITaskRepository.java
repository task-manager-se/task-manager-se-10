package ru.zolov.tm.api;

import java.util.Date;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface ITaskRepository extends IRepository<Task> {

  @NotNull Task persist(@NotNull String userId, @NotNull String id, @NotNull String name);

  @Nullable Task persist(@NotNull Task task);

  @Nullable Task findOne(@NotNull String userId, @NotNull String projectId, @NotNull String id)
  throws EmptyStringException, EmptyRepositoryException;

  @Nullable List<Task> findTaskByProjId(@NotNull String id, @NotNull String projectId)
  throws EmptyStringException, EmptyRepositoryException;

  @NotNull List<Task> findAll(@NotNull String userId) throws EmptyRepositoryException;

  @NotNull List<Task> findAll() throws EmptyRepositoryException;

  void update(@NotNull String userId,
              @NotNull String id,
              @NotNull String name,
              @NotNull String descriptionfinal,
              @NotNull Date start,
              @NotNull Date finish);

  boolean remove(@NotNull String userId, @NotNull String id);

  void removeAll(@NotNull String userId) throws EmptyRepositoryException;

  boolean removeAllByProjectID(@NotNull String userId, @NotNull String id);

  void merge(Task task);

  @Nullable List<Task> findTask(@NotNull String userId, @NotNull String partOfTheName)
  throws EmptyRepositoryException;

}
